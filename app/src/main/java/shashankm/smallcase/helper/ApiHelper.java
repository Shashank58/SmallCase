package shashankm.smallcase.helper;

import android.content.Context;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import shashankm.smallcase.R;
import shashankm.smallcase.model.SmallCase;
import shashankm.smallcase.model.SmallCaseHistoricalPoint;
import shashankm.smallcase.presenter.SmallCaseDetailPresenter.SmallCaseData;
import shashankm.smallcase.presenter.SmallCaseDetailPresenter.SmallCaseHistorics;
import shashankm.smallcase.util.ApiClient;
import shashankm.smallcase.util.AppConstants;

/**
 * Created by shashankm on 17/10/16.
 */

public class ApiHelper {
    private static final String TAG = ApiHelper.class.getSimpleName();

    public void getSmallCase(final Context context, final SmallCaseData smallCaseData, String scid) {
        RequestParams params = new RequestParams();
        params.put("scid", scid);
        ApiClient.get("smallcase", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if (!response.getBoolean("success")) {
                        smallCaseData.onFailure(AppConstants.ONLINE,
                                response.getString("errors"));
                        return;
                    }
                    extractSmallCase(response, smallCaseData);
                } catch (JSONException e) {
                    e.printStackTrace();
                    smallCaseData.onFailure(AppConstants.ONLINE,
                            context.getString(R.string.error_generic));
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString,
                                  Throwable throwable) {
                smallCaseData.onFailure(AppConstants.ONLINE,
                        context.getString(R.string.network_failure));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                  JSONObject errorResponse) {
                smallCaseData.onFailure(AppConstants.ONLINE,
                        context.getString(R.string.network_failure));
            }
        });
    }

    public void getHistoric(final Context context, final SmallCaseHistorics smallCaseHistorics,
                            String scid) {
        RequestParams params = new RequestParams();
        params.put("scid", scid);
        ApiClient.get("historical", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if (!response.getBoolean("success")) {
                        smallCaseHistorics.onError(AppConstants.ONLINE,
                                response.getString("errors"));
                        return;
                    }
                    extractHistoric(response, smallCaseHistorics);
                } catch (JSONException e) {
                    e.printStackTrace();
                    smallCaseHistorics.onError(AppConstants.ONLINE,
                            context.getString(R.string.error_generic));
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                  JSONObject errorResponse) {
                smallCaseHistorics.onError(AppConstants.ONLINE,
                        context.getString(R.string.network_failure));
            }
        });
    }

    private void extractHistoric(JSONObject response, SmallCaseHistorics smallCaseHistorics)
            throws JSONException {
        List<SmallCaseHistoricalPoint> pointList = new ArrayList<>();
        JSONObject data = response.getJSONObject("data");
        JSONArray points = data.getJSONArray("points");
        for (int i = 0; i < points.length(); i++) {
            SmallCaseHistoricalPoint point = new SmallCaseHistoricalPoint();
            JSONObject pointObj = points.getJSONObject(i);
            point.setDate(pointObj.getString("date"));
            point.setIndex(pointObj.getDouble("index"));
            pointList.add(point);
        }
        smallCaseHistorics.onResult(AppConstants.ONLINE, pointList);
    }

    private void extractSmallCase(JSONObject responseObj, SmallCaseData smallCaseData)
            throws JSONException {
        SmallCase smallCase = new SmallCase();
        JSONObject data = responseObj.getJSONObject("data");
        smallCase.setScid(data.getString("scid"));
        smallCase.setRationale(data.getString("rationale"));
        JSONObject info = data.getJSONObject("info");
        smallCase.setName(info.getString("name"));
        smallCase.setType(info.getString("type"));
        JSONObject stats = data.getJSONObject("stats");
        smallCase.setIndexValue(stats.getDouble("indexValue"));
        JSONObject returns = stats.getJSONObject("returns");
        smallCase.setMonthlyReturns(returns.getDouble("monthly"));
        smallCase.setYearlyReturns(returns.getDouble("yearly"));
        smallCaseData.onSuccess(AppConstants.ONLINE, smallCase);
    }
}
