package shashankm.smallcase.helper;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.AxisValueFormatter;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.realm.internal.android.ISO8601Utils;
import shashankm.smallcase.model.SmallCaseHistoricalPoint;

/**
 * Created by shashankm on 18/10/16.
 */

public class ValueFormatter implements AxisValueFormatter {
    private List<SmallCaseHistoricalPoint> values;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
    private static final ISO8601Utils df = new ISO8601Utils();

    public ValueFormatter(List<SmallCaseHistoricalPoint> values) {
        this.values = new ArrayList<>(values);
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        try {
            ParsePosition parsePosition = new ParsePosition(0);
            Date date = df.parse(values.get((int) value).getDate(), parsePosition);
            return sdf.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public int getDecimalDigits() {
        return 0;
    }
}
