package shashankm.smallcase.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.Realm.Transaction;
import io.realm.Realm.Transaction.OnError;
import shashankm.smallcase.model.SmallCase;
import shashankm.smallcase.model.SmallCaseHistoricalPoint;
import shashankm.smallcase.presenter.SmallCaseDetailPresenter.SmallCaseData;
import shashankm.smallcase.presenter.SmallCaseDetailPresenter.SmallCaseHistorics;
import shashankm.smallcase.util.AppConstants;

/**
 * Created by shashankm on 18/10/16.
 */

public class OfflineStorageHelper {
    private Realm mRealm;
    private SharedPreferences mPref;

    public OfflineStorageHelper(Context context) {
        Realm.init(context);
        mRealm = Realm.getDefaultInstance();
        mPref = context.getSharedPreferences(AppConstants.SHARED_PREF,
                Context.MODE_PRIVATE);
    }

    public void cacheSmallCase(SmallCase smallCase) {
        mRealm.beginTransaction();
        mRealm.copyToRealmOrUpdate(smallCase);
        mRealm.commitTransaction();
    }

    public void getSmallCase(final SmallCaseData smallCaseData, final String scid) {
        mRealm.executeTransactionAsync(new Transaction() {
            @Override
            public void execute(Realm realm) {
                SmallCase smallCase = realm.where(SmallCase.class).equalTo("scid", scid).findFirst();
                smallCaseData.onSuccess(AppConstants.OFFLINE, smallCase);
            }
        }, new OnError() {
            @Override
            public void onError(Throwable error) {
                smallCaseData.onFailure(AppConstants.OFFLINE, "");
            }
        });
    }

    public void cacheHistorics(String scid, String points) {
        Editor editor = mPref.edit();
        String pointsString = mPref.getString(scid, null);
        if (pointsString != null) {
            editor.remove(scid).commit();
        }
        editor.putString(scid, points);
        editor.apply();
    }

    public void getHistorics(SmallCaseHistorics smallCaseHistorics, String scid) {
        String pointsString = mPref.getString(scid, "");
        if (!pointsString.equals("")) {
            try {
                List<SmallCaseHistoricalPoint> points = new ArrayList<>();
                JSONArray jsonArray = new JSONArray(pointsString);
                for (int i = 0; i < jsonArray.length(); i++) {
                    SmallCaseHistoricalPoint historics = new SmallCaseHistoricalPoint();
                    JSONObject smallCaseObj = jsonArray.getJSONObject(i);
                    historics.setDate(smallCaseObj.getString("date"));
                    historics.setIndex(smallCaseObj.getDouble("index"));
                    points.add(historics);
                }
                smallCaseHistorics.onResult(AppConstants.OFFLINE, points);
            } catch (JSONException e) {
                e.printStackTrace();
                smallCaseHistorics.onError(AppConstants.OFFLINE, "");
            }
            return;
        }
        smallCaseHistorics.onError(AppConstants.OFFLINE, "");
    }
}
