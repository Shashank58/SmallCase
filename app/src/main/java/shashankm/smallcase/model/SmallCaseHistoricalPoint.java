package shashankm.smallcase.model;

import org.json.JSONException;
import org.json.JSONObject;

import static io.realm.log.RealmLog.trace;

/**
 * Created by shashankm on 17/10/16.
 */

public class SmallCaseHistoricalPoint {
    public String date;
    public double index;

    public void setDate(String date) {
        this.date = date;
    }

    public void setIndex(double index) {
        this.index = index;
    }

    public String getDate() {
        return this.date;
    }

    public double getIndex() {
        return index;
    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("date", this.date);
            obj.put("index", this.index);
        } catch (JSONException e) {
            trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }
}
