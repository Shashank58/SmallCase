package shashankm.smallcase.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by shashankm on 17/10/16.
 */

public class SmallCase extends RealmObject {
    @PrimaryKey
    private String scid;
    private String name, rationale, image, type;
    private double indexValue, monthlyReturns, yearlyReturns;

    public SmallCase() {
        //For realm.
    }

    public SmallCase(String scid, String image) {
        this.scid = scid;
        this.image = image;
    }

    public String getScid() {
        return this.scid;
    }

    public String getName() {

        return this.name;
    }

    public String getRationale() {
        return this.rationale;
    }

    public double getIndexValue() {
        return this.indexValue;
    }

    public double getMonthlyReturns() {
        return this.monthlyReturns;
    }

    public double getYearlyReturns() {
        return this.yearlyReturns;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setIndexValue(double indexValue) {
        this.indexValue = indexValue;
    }

    public void setMonthlyReturns(double monthlyReturns) {
        this.monthlyReturns = monthlyReturns;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRationale(String rationale) {
        this.rationale = rationale;
    }

    public void setYearlyReturns(double yearlyReturns) {
        this.yearlyReturns = yearlyReturns;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
