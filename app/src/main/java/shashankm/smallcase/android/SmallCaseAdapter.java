package shashankm.smallcase.android;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import shashankm.smallcase.R;
import shashankm.smallcase.model.SmallCase;

/**
 * Created by shashankm on 17/10/16.
 */

public class SmallCaseAdapter extends RecyclerView.Adapter<SmallCaseAdapter.SmallCaseViewHolder> {
    private List<SmallCase> mSmallCaseList;
    private Context mContext;

    public SmallCaseAdapter(Context context, List<SmallCase> smallCaseList) {
        this.mContext = context;
        this.mSmallCaseList = new ArrayList<>(smallCaseList);
    }

    @Override
    public SmallCaseAdapter.SmallCaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.small_case_card, parent, false);
        return new SmallCaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SmallCaseAdapter.SmallCaseViewHolder holder, int position) {
        final SmallCase smallCase = mSmallCaseList.get(position);
        Glide.with(mContext)
                .load(smallCase.getImage())
                .asBitmap()
                .into(holder.smallCase);
        holder.card.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SmallCaseDetailActivity.class);
                intent.putExtra(SmallCaseDetailActivity.SMALL_CASE_IMAGE, smallCase.getImage());
                intent.putExtra(SmallCaseDetailActivity.SMALL_CASE_ID, smallCase.getScid());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSmallCaseList.size();
    }

    class SmallCaseViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_small_case)
        ImageView smallCase;
        @BindView(R.id.card_small_case)
        CardView card;

        SmallCaseViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
