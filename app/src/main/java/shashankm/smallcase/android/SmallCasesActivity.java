package shashankm.smallcase.android;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import shashankm.smallcase.R;
import shashankm.smallcase.model.SmallCase;

public class SmallCasesActivity extends AppCompatActivity {
    @BindView(R.id.small_case_list)
    RecyclerView mSmallCaseList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_small_cases);
        ButterKnife.bind(this);

        mSmallCaseList.setLayoutManager(new GridLayoutManager(this, 2));
        mSmallCaseList.setHasFixedSize(true);
        createSmallCases();
    }

    private void createSmallCases() {
        List<SmallCase> smallCaseList = new ArrayList<>();
        smallCaseList.add(new SmallCase("SCMO_0002",
                "https://www.smallcase.com/images/smallcases/187/SCMO_0002.png"));
        smallCaseList.add(new SmallCase("SCMO_0003",
                "https://www.smallcase.com/images/smallcases/187/SCMO_0003.png"));
        smallCaseList.add(new SmallCase("SCMO_0006",
                "https://www.smallcase.com/images/smallcases/187/SCMO_0006.png"));
        smallCaseList.add(new SmallCase("SCNM_0003",
                "https://www.smallcase.com/images/smallcases/187/SCNM_0003.png"));
        smallCaseList.add(new SmallCase("SCNM_0007",
                "https://www.smallcase.com/images/smallcases/187/SCNM_0007.png"));
        smallCaseList.add(new SmallCase("SCNM_0008",
                "https://www.smallcase.com/images/smallcases/187/SCNM_0008.png"));
        smallCaseList.add(new SmallCase("SCNM_0009",
                "https://www.smallcase.com/images/smallcases/187/SCNM_0009.png"));
        mSmallCaseList.setAdapter(new SmallCaseAdapter(this, smallCaseList));
    }
}
