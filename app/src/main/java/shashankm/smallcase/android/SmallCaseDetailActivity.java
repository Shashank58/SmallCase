package shashankm.smallcase.android;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import shashankm.smallcase.R;
import shashankm.smallcase.helper.ValueFormatter;
import shashankm.smallcase.model.SmallCase;
import shashankm.smallcase.model.SmallCaseHistoricalPoint;
import shashankm.smallcase.presenter.SmallCaseDetailPresenter;
import shashankm.smallcase.presenter.SmallCaseDetailPresenter.SmallCaseData;
import shashankm.smallcase.presenter.SmallCaseDetailPresenter.SmallCaseHistorics;
import shashankm.smallcase.util.AppConstants;
import shashankm.smallcase.util.AppUtils;

public class SmallCaseDetailActivity extends AppCompatActivity implements SmallCaseData,
        SmallCaseHistorics, View.OnClickListener {
    public static final String SMALL_CASE_ID = "small_case_id";
    public static final String SMALL_CASE_IMAGE = "small_case_image";
    private SmallCaseDetailPresenter mSmallCaseDetailPresenter;
    @BindView(R.id.image_small_case_pic)
    ImageView smallCasePic;
    @BindView(R.id.text_small_case_name)
    TextView smallCaseName;
    @BindView(R.id.text_small_case_type)
    TextView smallCaseType;
    @BindView(R.id.text_small_case_index)
    TextView smallCaseIndex;
    @BindView(R.id.text_one_month_return)
    TextView oneMonthReturn;
    @BindView(R.id.text_one_year_return)
    TextView oneYearReturn;
    @BindView(R.id.text_rationale)
    TextView rationale;
    @BindView(R.id.chart)
    LineChart historicChart;
    @BindView(R.id.root_layout)
    CoordinatorLayout rootLayout;

    private String mScid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_small_case_detail);
        ButterKnife.bind(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Smallcase Details");
        }

        mSmallCaseDetailPresenter = new SmallCaseDetailPresenter(this, this, this);
        mScid = getIntent().getStringExtra(SMALL_CASE_ID);
        mSmallCaseDetailPresenter.fetchSmallCase(mScid);
        Glide.with(this)
                .load(getIntent().getStringExtra(SMALL_CASE_IMAGE))
                .asBitmap()
                .into(smallCasePic);
        mSmallCaseDetailPresenter.fetchHistoric(mScid);
    }

    /**
     * Called when details of a small case is successfully fetched
     * @param smallCase whose data needs to be displayed
     */
    @Override
    public void onSuccess(int flag, SmallCase smallCase) {
        if (flag == AppConstants.ONLINE) {
            mSmallCaseDetailPresenter.cacheSmallCase(smallCase);
        } else {
            AppUtils.getInstance().showSnackBar(rootLayout, this);
        }
        smallCaseName.setText(mSmallCaseDetailPresenter.getPresentableName(smallCase.getName()));
        smallCaseType.setText(mSmallCaseDetailPresenter.getPresentableType(smallCase.getType()));
        smallCaseIndex.setText(mSmallCaseDetailPresenter.getPresentableIndex(smallCase
                .getIndexValue()));
        oneMonthReturn.setText(mSmallCaseDetailPresenter.getPresentableMonthlyReturn(smallCase
                .getMonthlyReturns()));
        oneYearReturn.setText(mSmallCaseDetailPresenter.getPresentableYearlyReturn(smallCase
                .getYearlyReturns()));
        rationale.setText(mSmallCaseDetailPresenter.getPresentableRationale(smallCase
                .getRationale()));
    }

    /**
     * Failure on fetching small case info.
     * @param error message.
     */
    @Override
    public void onFailure(int flag, String error) {
        if (flag == AppConstants.OFFLINE) {
            AppUtils.getInstance().showSnackBar(rootLayout, this);
            return;
        }
        AppUtils.getInstance().showAlertDialog(this, error);
    }

    /**
     * Called when historic points of a small case has been fetched.
     * @param points year wise data of the small case.
     */
    @Override
    public void onResult(int flag, List<SmallCaseHistoricalPoint> points) {
        if (flag == AppConstants.ONLINE) {
            mSmallCaseDetailPresenter.cacheHistorics(mScid, points);
        }
        historicChart.setData(mSmallCaseDetailPresenter.setChartData(points));
        XAxis xAxis = historicChart.getXAxis();
        xAxis.setGranularity(25f);
        xAxis.setValueFormatter(new ValueFormatter(points));
        historicChart.setDescription("");
        historicChart.invalidate();
    }

    /**
     * Failure on fetching small case historic.
     * @param error message.
     */
    @Override
    public void onError(int flag, String error) {
        if (flag == AppConstants.OFFLINE) return;
        AppUtils.getInstance().showAlertDialog(this, error);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (AppUtils.getInstance().isNetworkAvailable(this)) {
            mSmallCaseDetailPresenter.fetchSmallCase(mScid);
            mSmallCaseDetailPresenter.fetchHistoric(mScid);
        } else {
            AppUtils.getInstance().showSnackBar(rootLayout, this);
        }
    }
}
