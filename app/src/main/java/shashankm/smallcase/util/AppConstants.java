package shashankm.smallcase.util;

/**
 * Created by shashankm on 18/10/16.
 */

public class AppConstants {
    public static final int OFFLINE = 1;
    public static final int ONLINE = 2;
    public static final String SHARED_PREF = "small_case";
    public static final int PRIVATE_MODE_PREFERENCE = 10;
}
