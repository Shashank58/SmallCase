package shashankm.smallcase.util;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.View;

import shashankm.smallcase.R;

/**
 * Created by shashankm on 17/10/16.
 */

public class AppUtils {
    private static AppUtils instance;

    public static AppUtils getInstance(){
        if (instance == null){
            instance = new AppUtils();
        }
        return instance;
    }

    public boolean isNetworkAvailable(Context context){
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void showAlertDialog(Context context, String message){
        new AlertDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).create().show();
    }

    public void showSnackBar(CoordinatorLayout rootLayout, View.OnClickListener listener) {
        Snackbar snackbar = Snackbar
                .make(rootLayout, "No internet connection, data not in sync.",
                        Snackbar.LENGTH_INDEFINITE)
                .setAction("Retry", listener);

        snackbar.show();
    }
}
