package shashankm.smallcase.presenter;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;

import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import shashankm.smallcase.helper.ApiHelper;
import shashankm.smallcase.helper.OfflineStorageHelper;
import shashankm.smallcase.model.SmallCase;
import shashankm.smallcase.model.SmallCaseHistoricalPoint;
import shashankm.smallcase.util.AppUtils;

/**
 * Created by shashankm on 17/10/16.
 */

public class SmallCaseDetailPresenter {
    private Context mContext;
    private SmallCaseData mSmallCaseData;
    private ApiHelper mApiHelper;
    private SmallCaseHistorics mSmallCaseHistorics;
    private OfflineStorageHelper mOfflineStorageHelper;
    private static final String TAG = SmallCaseDetailPresenter.class.getSimpleName();

    public interface SmallCaseData {
        void onSuccess(int flag, SmallCase smallCase);
        void onFailure(int flag, String error);
    }

    public interface SmallCaseHistorics {
        void onResult(int flag, List<SmallCaseHistoricalPoint> points);
        void onError(int flag, String error);
    }

    public SmallCaseDetailPresenter(Context context, SmallCaseData smallCaseData,
                                    SmallCaseHistorics smallCaseHistorics) {
        this.mContext = context;
        this.mSmallCaseData = smallCaseData;
        this.mSmallCaseHistorics = smallCaseHistorics;
        mApiHelper = new ApiHelper();
        mOfflineStorageHelper= new OfflineStorageHelper(mContext);
    }

    public SpannableString getPresentableName(String smallCaseName) {
        String name = "Name: " + smallCaseName;
        SpannableString spannableString = new SpannableString(name);
        spannableString.setSpan(new RelativeSizeSpan(1.2f), 0, 5, 0);
        return spannableString;
    }

    public SpannableString getPresentableType(String smallCaseType) {
        String type = "Type: " + smallCaseType;
        SpannableString spannableString = new SpannableString(type);
        spannableString.setSpan(new RelativeSizeSpan(1.2f), 0, 5, 0);
        return spannableString;
    }

    public SpannableString getPresentableIndex(double indexValue) {
        String index = "Index: " + String.format(Locale.getDefault(), "%.2f", indexValue);
        SpannableString spannableString = new SpannableString(index);
        spannableString.setSpan(new RelativeSizeSpan(1.2f), 0, 6, 0);
        return spannableString;
    }

    public SpannableString getPresentableMonthlyReturn(double monthlyReturns) {
        String monthly = "Monthly: " + String.format(Locale.getDefault(), "%.2f", monthlyReturns)
                + " %";
        SpannableString spannableString = new SpannableString(monthly);
        spannableString.setSpan(new RelativeSizeSpan(1.2f), 0, 8, 0);
        return spannableString;
    }

    public SpannableString getPresentableYearlyReturn(double yearlyReturns) {
        String yearly = "Yearly: " + String.format(Locale.getDefault(), "%.2f", yearlyReturns)
                + " %";
        SpannableString spannableString = new SpannableString(yearly);
        spannableString.setSpan(new RelativeSizeSpan(1.2f), 0, 7, 0);
        return spannableString;
    }

    public SpannableString getPresentableRationale(String rationale) {
        String rationaleText = "Rationale\n\n" + Html.fromHtml(rationale);
        SpannableString spannableString = new SpannableString(rationaleText);
        spannableString.setSpan(new RelativeSizeSpan(2f), 0, 9, 0);
        spannableString.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 9, 0);
        return spannableString;
    }

    public void cacheSmallCase(SmallCase smallCase) {
        mOfflineStorageHelper.cacheSmallCase(smallCase);
    }

    /**
     * Convert points to string and store in shared pref as opposed to storing in realm db
     * to save space.
     * @param scid used as key to store in shared pref.
     * @param points list of all historic points of a small case.
     */
    public void cacheHistorics(String scid, List<SmallCaseHistoricalPoint> points) {
        JSONArray jsonArray = new JSONArray();
        for (SmallCaseHistoricalPoint point : points) {
            jsonArray.put(point.getJSONObject());
        }
        mOfflineStorageHelper.cacheHistorics(scid, jsonArray.toString());
    }

    public void fetchSmallCase(String scid) {
        if (AppUtils.getInstance().isNetworkAvailable(mContext)) {
            mApiHelper.getSmallCase(mContext, mSmallCaseData, scid);
        } else {
            mOfflineStorageHelper.getSmallCase(mSmallCaseData, scid);
        }
    }

    public void fetchHistoric(String scid) {
        if (AppUtils.getInstance().isNetworkAvailable(mContext)) {
            mApiHelper.getHistoric(mContext, mSmallCaseHistorics, scid);
        } else {
            mOfflineStorageHelper.getHistorics(mSmallCaseHistorics, scid);
        }
    }

    public LineData setChartData(List<SmallCaseHistoricalPoint> points) {
        float quarter = 0f;
        List<Entry> historicList = new ArrayList<>();
        for (SmallCaseHistoricalPoint point : points) {
            Entry entry = new Entry(quarter, (float) point.getIndex());
            historicList.add(entry);
            quarter = quarter + 1f;
        }
        LineDataSet lineDataSet = new LineDataSet(historicList, "Stocks");
        lineDataSet.setAxisDependency(AxisDependency.LEFT);
        return new LineData(lineDataSet);
    }
}
